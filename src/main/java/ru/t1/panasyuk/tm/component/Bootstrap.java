package ru.t1.panasyuk.tm.component;

import ru.t1.panasyuk.tm.api.repository.ICommandRepository;
import ru.t1.panasyuk.tm.api.repository.IProjectRepository;
import ru.t1.panasyuk.tm.api.repository.ITaskRepository;
import ru.t1.panasyuk.tm.api.repository.IUserRepository;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.command.AbstractCommand;
import ru.t1.panasyuk.tm.command.project.*;
import ru.t1.panasyuk.tm.command.system.*;
import ru.t1.panasyuk.tm.command.task.*;
import ru.t1.panasyuk.tm.command.user.*;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.panasyuk.tm.exception.system.CommandNotSupportedException;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.repository.CommandRepository;
import ru.t1.panasyuk.tm.repository.ProjectRepository;
import ru.t1.panasyuk.tm.repository.TaskRepository;
import ru.t1.panasyuk.tm.repository.UserRepository;
import ru.t1.panasyuk.tm.service.*;
import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository, projectTaskService);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationExitCommand());
        registry(new ApplicationAboutCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationInfoCommand());
        registry(new ApplicationVersionCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProject());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
        registry(new UserRemoveCommand());
    }

    private void exit() {
        System.exit(0);
    }

    private void initDemoData() {
        final User user1 = userService.create("PPANASYUK", "qwerty", "ppanasyuk@t1-consulting.ru");
        final User user2 = userService.create("SADMIN", "123", Role.ADMIN);

        final Project project1 = projectService.create(user1.getId(), "Project 1", "Project for ppanasyuk");
        final Project project2 = projectService.create(user2.getId(), "Project 2", "Project from sadmin");

        taskService.create(user1.getId(), "Task 1", "Task for project 1").setProjectId(project1.getId());
        taskService.create(user1.getId(), "Task 2", "Task for project 1").setProjectId(project1.getId());
        taskService.create(user2.getId(), "Task 3", "Task for project 2").setProjectId(project2.getId());
        taskService.create(user2.getId(), "Task 4", "Task for project 2").setProjectId(project2.getId());
        taskService.create(user2.getId(), "Task 5", "Test task");
    }

    private void initLogger() {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("*** TASK MANAGER IS SHUTTING DOWN ***");
            }
        });
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("\nENTER COMMAND: ");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(final String... args) {
        processArguments(args);
        initDemoData();
        initLogger();
        processCommands();
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

}