package ru.t1.panasyuk.tm.api.model;

import ru.t1.panasyuk.tm.enumerated.Role;

public interface ICommand {

    void execute();

    String getArgument();

    String getDescription();

    String getName();

    Role[] getRoles();

    String getUserId();

}