package ru.t1.panasyuk.tm.api.service;

import ru.t1.panasyuk.tm.api.repository.IUserRepository;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.model.User;

public interface IUserService extends IService<User>, IUserRepository {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    void lockUserByLogin(String login);

    User removeByLogin(String login);

    User removeByEmail(String email);

    User setPassword(String id, String password);

    void unlockUserByLogin(String login);

    User updateUser(String id, String firstName, String lastName, String middleName);

}