package ru.t1.panasyuk.tm.command.project;

import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    private static final String DESCRIPTION = "Remove project by id.";

    private static final String NAME = "project-remove-by-id";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectTaskService().removeProjectById(userId, id);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}