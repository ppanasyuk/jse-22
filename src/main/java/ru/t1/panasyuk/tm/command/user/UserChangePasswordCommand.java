package ru.t1.panasyuk.tm.command.user;

import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    private final String NAME = "user-change-password";

    private final String DESCRIPTION = "Change password of current user.";

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}