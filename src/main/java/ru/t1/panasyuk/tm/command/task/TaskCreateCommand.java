package ru.t1.panasyuk.tm.command.task;

import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    private static final String DESCRIPTION = "Create new task.";

    private static final String NAME = "task-create";

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().create(userId, name, description);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}